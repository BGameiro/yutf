---
title: Home
subtitle: This is where you landed
comments: false
---


# BGameiro's website

This is my website where you can find my articles, the projects I'm involved, my CV and much more. 












##### Attention

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
and [Hugo](https://gohugo.io) using [panr](https://twitter.com/panr)'s theme.