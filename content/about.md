---
title: About
subtitle: $whoami
comments: false
---

My name is Bernardo Gameiro.

I'm currently taking a Bachelor of Science in Engineering Physics from the Faculty of Sciences, University of Lisbon and a Minor Degree in Informatics.

I wave a wide range of interests including, but not limited to:

- Engineering & Physics
  - Plasma
  - Instumentation
  - Robotics
  - CAD

- Informatics & Computers
  - Automation
  - Programming
  - Computer Networks
  - Servers
  - Open Source Software
  - Linux

Looking forward to learn thru internships, online learning and conferences.